This project is created to expand my knowledge of React; Redux; third-party API (movie database); Firebase and deployment while building real-world full-stack application. 

This project is live at https://list-proj.herokuapp.com (please give Heroku few seconds to run it's server).

It is still a bit buggy, but most of these bugs are in my backlog (as well as new features) and hopefully I will fix them soon.

Feel free to use this code for education purposes.

Please ask me questions via y.piddubnyak@gmail.com
