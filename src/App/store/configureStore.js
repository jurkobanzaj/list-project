import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import authReducer from '../reducers/auth';
import listFilterReducer from '../reducers/listFilterReducer';
import listReducer from '../reducers/listReducer';
import serviceReducer from '../reducers/serviceReducer';

const allReducers = combineReducers({
    auth: authReducer,
    filter: listFilterReducer,
    list: listReducer,
    service: serviceReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
    const store = createStore(
        allReducers,
        {
            list: []
        },
        composeEnhancers(applyMiddleware(thunk))
    );
    return store;
};
