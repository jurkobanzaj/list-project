import database from '../firebase/firebase';

export const headingEdit = header => ({
    type: 'HEADING_EDIT',
    header
});

export const startHeadingEdit = header => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        return database
            .ref(`users/${uid}/service/heading`)
            .update({ header: header })
            .then(() => {
                dispatch(headingEdit(header));
            });
    };
};

export const setService = service => ({
    type: 'SET_SERVICE',
    service
});

export const startSetService = () => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        return database
            .ref(`users/${uid}/service`)
            .once('value')
            .then(snapshot => {
                var service = {};
                snapshot.forEach(childShapshot => {
                    service = {
                        ...service,
                        ...childShapshot.val()
                    };
                });
                dispatch(setService(service));
            });
    };
};
