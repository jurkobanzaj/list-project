export const setTextFilter = (text = '') => ({
    type: 'SET_TEXT_FILTER',
    text
});

export const sortByTitle = () => ({
    type: 'SORT_BY_TITLE'
});

export const sortByYear = () => ({
    type: 'SORT_BY_YEAR'
});

export const sortBySeen = () => ({
    type: 'SORT_BY_SEEN'
});

export const sortByRating = () => ({
    type: 'SORT_BY_RATING'
});

export const setStartYear = startYear => ({
    type: 'SET_START_YEAR',
    startYear
});

export const setEndYear = endYear => ({
    type: 'SET_END_YEAR',
    endYear
});
