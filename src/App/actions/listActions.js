import database from '../firebase/firebase';

export const addListItem = listItem => ({
    type: 'ADD_LIST_ITEM',
    listItem
});

export const startAddListItem = listItemData => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        const {
            title = '',
            year = '1895',
            genre = 'N/A',
            image = 'N/A',
            rating = 0,
            actors = 'N/A',
            director = 'N/A',
            plot = 'N/A',
            seen = false
        } = listItemData;
        const listItem = { title, year, genre, image, rating, actors, director, plot, seen };
        database
            .ref(`users/${uid}/movies`)
            .push(listItem)
            .then(ref => {
                dispatch(
                    addListItem({
                        id: ref.key,
                        ...listItem
                    })
                );
            });
    };
};

export const checkboxChange = id => ({
    type: 'CHECKBOX_CHANGE',
    id
});

export const startCheckboxChange = id => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        return database
            .ref(`users/${uid}/movies/${id}/seen`)
            .once('value')
            .then(snapshot => {
                database
                    .ref(`users/${uid}/movies/${id}/seen`)
                    .set(!snapshot.val())
                    .then(() => {
                        dispatch(checkboxChange(id));
                    });
            });
    };
};

export const removeListItem = id => ({
    type: 'REMOVE_LIST_ITEM',
    id
});

export const startRemoveListItem = id => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        return database
            .ref(`users/${uid}/movies/${id}`)
            .remove()
            .then(() => {
                dispatch(removeListItem(id));
            });
    };
};

export const saveListItem = (id, listItem) => ({
    type: 'SAVE_LIST_ITEM',
    id,
    listItem
});

export const startSaveListItem = (id, listItemData) => {
    return (dispatch, getState) => {
        const {
            title = '',
            year = '1895',
            genre = 'N/A',
            image = 'N/A',
            rating = 0,
            actors = 'N/A',
            director = 'N/A',
            plot = 'N/A',
            seen = false
        } = listItemData;
        const listItem = { title, year, genre, image, rating, actors, director, plot, seen };
        const uid = getState().auth.uid;
        return database
            .ref(`users/${uid}/movies/${id}`)
            .update(listItem)
            .then(() => {
                dispatch(saveListItem(id, listItem));
            });
    };
};

export const setList = list => ({
    type: 'SET_LIST',
    list
});

export const startSetList = () => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        return database
            .ref(`users/${uid}/movies`)
            .once('value')
            .then(snapshot => {
                const list = [];
                snapshot.forEach(childShapshot => {
                    list.push({
                        id: childShapshot.key,
                        ...childShapshot.val()
                    });
                });
                dispatch(setList(list));
            });
    };
};
