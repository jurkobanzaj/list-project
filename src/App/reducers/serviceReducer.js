const serviceReucerDefaultState = {
    heading: 'My want to see movies list'
};

export default (state = serviceReucerDefaultState, action) => {
    switch (action.type) {
        case 'HEADING_EDIT':
            return {
                ...state,
                heading: action.heading
            };
        case 'SET_SERVICE':
            return action.service;
        default:
            return state;
    }
};
