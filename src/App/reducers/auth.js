export default (state = {}, action) => {
    switch (action.type) {
        case 'LOGIN':
            return {
                displayName: action.displayName,
                uid: action.uid
            };
        case 'LOGOUT':
            return {};
        default:
            return state;
    }
};
