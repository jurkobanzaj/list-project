const filterReducerDefaultState = {
    text: '',
    sortBy: 'rating',
    startYear: undefined,
    endYear: undefined
};

export default (state = filterReducerDefaultState, action) => {
    switch (action.type) {
        case 'SET_TEXT_FILTER':
            return {
                ...state,
                text: action.text
            };
        case 'SORT_BY_TITLE':
            return {
                ...state,
                sortBy: 'title'
            };
        case 'SORT_BY_YEAR':
            return {
                ...state,
                sortBy: 'year'
            };
        case 'SORT_BY_SEEN':
            return {
                ...state,
                sortBy: 'seen'
            };
        case 'SORT_BY_RATING':
            return {
                ...state,
                sortBy: 'rating'
            };
        case 'SET_START_YEAR':
            return {
                ...state,
                startYear: action.startYear
            };
        case 'SET_END_YEAR':
            return {
                ...state,
                endYear: action.endYear
            };
        default:
            return state;
    }
};
