export default (state = [], action) => {
    switch (action.type) {
        case 'ADD_LIST_ITEM':
            return [
                ...state,
                {
                    id: action.listItem.id,
                    title: action.listItem.title,
                    year: action.listItem.year,
                    genre: action.listItem.genre,
                    image: action.listItem.image,
                    rating: action.listItem.rating,
                    actors: action.listItem.actors,
                    director: action.listItem.director,
                    plot: action.listItem.plot,
                    seen: action.listItem.seen
                }
            ];
        case 'CHECKBOX_CHANGE':
            return state.map(listItem => {
                if (listItem.id === action.id) {
                    return {
                        ...listItem,
                        seen: !listItem.seen
                    };
                }
                return listItem;
            });
        case 'REMOVE_LIST_ITEM':
            return state.filter(({ id }) => {
                // if id matches, returns new array
                return id !== action.id;
            });
        case 'SAVE_LIST_ITEM':
            return state.map(listItem => {
                // maps state
                if (listItem.id === action.id) {
                    // looks for id
                    return {
                        ...listItem,
                        ...action.listItem // updates item with provided id
                    };
                }
                return listItem;
            });
        case 'SET_LIST':
            return action.list;
        default:
            return state;
    }
};
