export const getFilteredList = (list, { text, sortBy, startYear, endYear }) => {
    return list
        .filter(listItem => {
            const textMatch = listItem.title.toLowerCase().includes(text.toLowerCase());
            const startYearMatch = typeof startYear !== 'number' || listItem.year >= startYear;
            const endYearMatch = typeof endYear !== 'number' || listItem.year <= endYear;

            return textMatch && startYearMatch && endYearMatch;
        })
        .sort((a, b) => {
            if (sortBy === 'year') {
                return a.year < b.year ? 1 : -1;
            }
            if (sortBy === 'title') {
                return a.title < b.title ? -1 : 1;
            }
            if (sortBy === 'rating') {
                return a.rating < b.rating ? 1 : -1;
            }
            if (sortBy === 'seen') {
                return a.seen < b.seen ? 1 : -1;
            }
            return null;
        });
};
