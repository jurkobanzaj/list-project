import React from 'react';
import { connect } from 'react-redux';
import { startLogout } from '../../actions/auth';
import './Header.scss';

const Header = ({ user, startLogout }) => (
    <div className="row">
        <div className="section">
            Howdy, <strong>{user}</strong>! Not {user}?{' '}
            <a onClick={startLogout} className="Header-logout">
                Log Out
            </a>
        </div>
        <div className="section">
            <div className="right-align">
                <span>
                    "Don't ever let someone tell you, you can't do something. You got a dream, you got to protect it."
                </span>
                <br />
                <span>
                    <em>Chris Gardner, The Pursuit of Happyness (2006)</em>
                </span>
            </div>
        </div>
    </div>
);

const mapDispatchToProps = dispatch => ({
    startLogout: () => dispatch(startLogout())
});

export default connect(
    undefined,
    mapDispatchToProps
)(Header);
