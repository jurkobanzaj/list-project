import React from 'react';
import ListItemForm from '../ListItemForm/ListItemForm';

export default class AddListItem extends React.Component {
    constructor(props) {
        super(props);

        this.addListRef = React.createRef();

        this.state = {
            listItem: {
                title: '',
                year: '',
                genre: '',
                image: '',
                rating: '',
                actors: '',
                director: '',
                plot: '',
                seen: false
            },
            isAdding: false
        };
    }

    componentDidUpdate() {
        if (this.addListRef.current) {
            window.scrollTo(0, this.addListRef.current.offsetTop);
        }
        return;
    }

    render() {
        return <div>{this._renderAddSection()}</div>;
    }

    _renderAddSection = () => {
        if (this.state.isAdding) {
            return (
                <div className="AddMovie-container" ref={this.addListRef}>
                    <h4>Add Movie</h4>
                    <ListItemForm
                        onFormSubmit={this._onFormSubmit}
                        listItem={this.state.listItem}
                        onCheckboxChange={this._onCheckboxChange}
                        onCancelClick={this._onCancelClick}
                    />
                </div>
            );
        }
        return (
            <div>
                <button
                    className="waves-effect waves-light btn-small light-blue lighten-2"
                    onClick={this._onAddButtonClick}
                >
                    Add Movie
                </button>
            </div>
        );
    };

    _onAddButtonClick = () => {
        this.setState({
            isAdding: true
        });
    };

    _onCancelClick = () => {
        this.setState({
            isAdding: false
        });
    };

    _onFormSubmit = event => {
        event.preventDefault();
        const listItem = {
            title: event.target.elements.title.value,
            year: event.target.elements.year.value,
            genre: event.target.elements.genre.value,
            image: event.target.elements.image.value,
            rating: event.target.elements.rating.value,
            actors: event.target.elements.actors.value,
            director: event.target.elements.director.value,
            plot: event.target.elements.plot.value,
            seen: this.state.listItem.seen
        };
        this.props.onAddListItem(listItem);
        this.setState({
            listItem: {},
            isAdding: false
        });
    };

    _onCheckboxChange = () => {
        this.setState(state => ({
            listItem: {
                ...state.listItem,
                seen: !state.listItem.seen
            }
        }));
    };
}
