import React from 'react';
import SearchDropdown from './SearchDropdown';
import { API_SEARCH_URL, API_GET_MOVIE_DATA_BY_IMDB_ID_URL, API_KEY } from '../../../settings/api';
import { debounce } from '../../../utils/heplers';

var debounceAPIRequest = debounce(function(func, text) {
    func(text);
}, 1000);

class ListItemForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            title: this.props.listItem.title,
            foundOptions: [],
            databaseError: '',
            userInput: '',
            year: this.props.listItem.year,
            genre: this.props.listItem.genre,
            image: this.props.listItem.image,
            rating: this.props.listItem.rating,
            actors: this.props.actors,
            director: this.props.director,
            plot: this.props.plot,
            seen: this.props.listItem.seen
        };
    }

    render() {
        return (
            <form onSubmit={this.props.onFormSubmit} autoComplete="off">
                <div>
                    <label htmlFor="title">Title*</label>
                    <SearchDropdown
                        onChange={this._onTitleChange}
                        searchQuery={this.state.userInput || this.state.title}
                        onSelectOption={this._onSelectOption}
                        foundOptions={this.state.foundOptions}
                        databaseError={this.state.databaseError}
                    />
                    <br />
                    <label htmlFor="year">Year</label>
                    <input type={'text'} id="year" name="year" defaultValue={this.state.year} />
                    <label htmlFor="genre">Genre</label>
                    <input type={'text'} id="genre" name="genre" defaultValue={this.state.genre} />
                    <input hidden type={'text'} id="image" name="image" defaultValue={this.state.image} />
                    <input hidden type={'text'} name="rating" defaultValue={this.state.rating} />
                    <input hidden type={'text'} name="actors" defaultValue={this.state.actors} />
                    <input hidden type={'text'} name="director" defaultValue={this.state.director} />
                    <input hidden type={'text'} name="plot" defaultValue={this.state.plot} />
                    <br />
                    <label>
                        <input
                            type="checkbox"
                            checked={this.state.seen ? 'checked' : ''}
                            onChange={this._onCheckboxChange}
                        />
                        <span>Seen this movie?</span>
                    </label>
                </div>
                <div className="ListItemForm-buttonGroup">
                    <button className="waves-effect waves-light btn-small light-blue lighten-2">Save</button>
                    <button
                        onClick={this.props.onCancelClick}
                        className="waves-effect waves-light btn-small red lighten-3"
                    >
                        Cancel
                    </button>
                </div>
            </form>
        );
    }

    _searchItemInDB = itemTitle => {
        if (itemTitle.length > 1) {
            const searchRequest = API_SEARCH_URL + itemTitle + '&type=movie' + API_KEY;
            fetch(searchRequest)
                .then(response => response.json())
                .then(data => {
                    if (data.Response === 'True') {
                        this.setState({
                            foundOptions: data.Search,
                            databaseError: ''
                        });
                    } else {
                        this.setState({
                            databaseError: data.Error,
                            foundOptions: []
                        });
                    }
                });
        }
    };

    _onSelectOption = imdbId => {
        if (imdbId) {
            const searchRequest = API_GET_MOVIE_DATA_BY_IMDB_ID_URL + imdbId + API_KEY;
            fetch(searchRequest)
                .then(response => response.json())
                .then(data => {
                    if (data.Response === 'True') {
                        this.setState({
                            userInput: data.Title,
                            genre: data.Genre,
                            image: data.Poster,
                            rating: data.imdbRating,
                            year: data.Year,
                            actors: data.Actors,
                            director: data.Director,
                            plot: data.Plot,
                            foundOptions: []
                        });
                    }
                    return false;
                });
        }
    };

    _onTitleChange = event => {
        const searchQuery = event.target.value;
        this.setState({
            databaseError: '',
            foundOptions: [],
            userInput: searchQuery
        });
        debounceAPIRequest(this._searchItemInDB, searchQuery);
    };

    _onCheckboxChange = () => {
        this.props.onCheckboxChange(this.props.listItem.id);
        this.setState(state => ({
            seen: !state.seen
        }));
    };
}

export default ListItemForm;
