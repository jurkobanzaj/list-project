import React from 'react';
import './SearchDropdown.scss';

export default function SearchDropdown({ onChange, searchQuery, onSelectOption, foundOptions, databaseError }) {
    return (
        <React.Fragment>
            <input type={'text'} name="title" onChange={onChange} required value={searchQuery} />
            <div className="SearchDropdown-optionsContainer">
                {foundOptions.map((item, i) => {
                    if (item.Title && searchQuery) {
                        return (
                            <div
                                className="SearchDropdown-option"
                                onClick={() => onSelectOption(item.imdbID)}
                                key={`${item.Title}, ${i}`}
                            >
                                {`${item.Title}, ${item.Year}`}
                            </div>
                        );
                    }
                    return null;
                })}
                {databaseError ? <div className="SearchDropdown-error">Database says: {databaseError}</div> : null}
            </div>
        </React.Fragment>
    );
}
