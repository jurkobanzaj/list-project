import React from 'react';
import { connect } from 'react-redux';
import ListItem from './ListItem/ListItem';
import Header from '../Header/Header';
import AddListItem from './AddListItem/AddListItem';
import Footer from '../Footer/Footer';

import { getFilteredList } from '../../utils/filter';
import './Home.scss';

import {
    startAddListItem,
    startCheckboxChange,
    startRemoveListItem,
    startSaveListItem
} from '../../actions/listActions';

import { setTextFilter, sortByTitle, sortByYear, sortBySeen, sortByRating } from '../../actions/listFilterActions';

import { startHeadingEdit } from '../../actions/serviceActions';

export class Home extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            header: this.props.service.header || 'My movie list',
            isHeadingBeaingEdited: false,
            search: '',
            sorting: null
        };
    }

    render() {
        return (
            <div className="Home-container">
                <Header user={this.props.auth.displayName} />
                {this._renderHeader()}
                <div>
                    <div>
                        <input
                            type="text"
                            value={this.state.search}
                            placeholder="Search in titles"
                            onChange={this._onSearchChange}
                        />
                    </div>
                    <div>
                        <AddListItem onAddListItem={this._onAddListItem} />
                    </div>
                </div>
                {this.props.list.length === 0 ? (
                    <div>
                        <span>To add items to this list hit Add Item!</span>
                    </div>
                ) : (
                    this._renderList()
                )}
                <AddListItem onAddListItem={this._onAddListItem} />
                <Footer />
            </div>
        );
    }

    _renderList = () => {
        const searchResults = getFilteredList(this.props.list, this.props.filter);
        return (
            <div className="section">
                <ul className="highlight">
                    <li>
                        <div className="Home-tableHeading row">
                            <span className="Home-columnHeading col s3 m4" onClick={this._onSortByTitle}>
                                Title{this._renderSortingArrow('title')}
                            </span>
                            <span className="Home-columnHeading col s1 m1" onClick={this._onSortByYear}>
                                Year{this._renderSortingArrow('year')}
                            </span>
                            <span className="Home-columnHeading col s2 m2 isDesktop">Genre</span>
                            <span className="Home-columnHeading col s2 m1" onClick={this._onSortByRating}>
                                IMDB Rating{this._renderSortingArrow('rating')}
                            </span>
                            <span className="Home-columnHeading col s2 m1" onClick={this._onSortBySeen}>
                                Seen{this._renderSortingArrow('seen')}
                            </span>
                            <span className="Home-columnHeading col s2 m3">Actions</span>
                        </div>
                    </li>
                    {searchResults.map((listItem, i) => {
                        return (
                            <ListItem
                                key={listItem.id}
                                index={i}
                                listItem={listItem}
                                onRemoveListItem={this._onRemoveListItem}
                                onSaveListItem={this._onSaveListItem}
                                onCheckboxChange={this._onCheckboxChange}
                            />
                        );
                    })}
                </ul>
            </div>
        );
    };

    _renderSortingArrow = sortingField => {
        if (this.state.sorting === sortingField) {
            return <i className="Home-sortingArrow tiny material-icons">arrow_drop_down</i>;
        }
        return null;
    };

    _renderHeader = () => {
        return (
            <div>
                {!this.state.isHeadingBeaingEdited ? (
                    <h3 className="Home-heading">
                        {this.state.header || this.props.service.header}
                        <i className="Home-editHeadingIcon material-icons" onClick={this._onHeadingEditClick}>
                            create
                        </i>
                    </h3>
                ) : (
                    <div>
                        <input
                            className="Home-headingInput"
                            type="text"
                            id="editHeadingInput"
                            defaultValue={this.state.header || this.props.service.header}
                            onKeyPress={this._onEnterClick}
                            onBlur={this._onHeadingEditBlur}
                        />
                        <label htmlFor="editHeadingInput">
                            <i className="Home-saveHeadingIcon material-icons" onClick={this._onHeadingEditClick}>
                                check
                            </i>
                        </label>
                    </div>
                )}
            </div>
        );
    };

    _onEnterClick = event => {
        if (event.key === 'Enter') {
            this._onHeadingEditBlur(event);
        }
    };

    _onHeadingEditClick = () => {
        this.setState({
            isHeadingBeaingEdited: !this.state.isHeadingBeaingEdited
        });
    };

    _onHeadingEdit = event => {
        this.props.startHeadingEdit(event.target.value);
        this.setState({
            header: event.target.value
        });
    };

    _onHeadingEditBlur = event => {
        this.props.startHeadingEdit(event.target.value);
        this.setState({
            header: event.target.value,
            isHeadingBeaingEdited: !this.state.isHeadingBeaingEdited
        });
    };

    _onSortByTitle = () => {
        this.props.sortByTitle();
        this.setState({
            sorting: 'title'
        });
    };

    _onSortByYear = () => {
        this.props.sortByYear();
        this.setState({
            sorting: 'year'
        });
    };

    _onSortBySeen = () => {
        this.props.sortBySeen();
        this.setState({
            sorting: 'seen'
        });
    };

    _onSortByRating = () => {
        this.props.sortByRating();
        this.setState({
            sorting: 'rating'
        });
    };
    _onCheckboxChange = id => {
        this.props.startCheckboxChange(id);
    };

    _onSaveListItem = (id, listItem) => {
        this.props.startSaveListItem(id, listItem);
    };

    _onRemoveListItem = id => {
        this.props.startRemoveListItem(id);
    };

    _onAddListItem = listItem => {
        this.props.startAddListItem(listItem);
    };

    _onSearchChange = event => {
        this.props.setTextFilter(event.target.value);
        this.setState({
            search: event.target.value
        });
    };
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
        filter: state.filter,
        service: state.service,
        list: state.list
    };
};

const mapDispatchToProps = dispatch => ({
    startHeadingEdit: header => dispatch(startHeadingEdit(header)),

    startAddListItem: listItem => dispatch(startAddListItem(listItem)),
    startCheckboxChange: id => dispatch(startCheckboxChange(id)),
    startRemoveListItem: id => dispatch(startRemoveListItem(id)),
    startSaveListItem: (id, listItem) => dispatch(startSaveListItem(id, listItem)),

    sortByRating: () => dispatch(sortByRating()),
    sortBySeen: () => dispatch(sortBySeen()),
    sortByTitle: () => dispatch(sortByTitle()),
    sortByYear: () => dispatch(sortByYear()),
    setTextFilter: text => dispatch(setTextFilter(text))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);
