import React from 'react';
import ListItemForm from '../ListItemForm/ListItemForm';
import ListItemDetails from '../ListItemDetails/ListItemDetails';
import './ListItem.scss';

export default class ListItem extends React.Component {
    constructor(props) {
        super(props);

        this.editListRef = React.createRef();
        this.viewListRef = React.createRef();

        this.state = {
            listItem: {},
            isEditing: false,
            isDeleting: false,
            isItemDetailsCardRendered: false
        };
    }

    componentDidMount() {
        document.addEventListener('keydown', this._onEscapeClick, false);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this._onEscapeClick, false);
    }

    componentDidUpdate() {
        if (this.editListRef.current) {
            window.scrollTo(0, this.editListRef.current.offsetTop);
        }
        return;
    }

    renderListItem() {
        if (this.state.isEditing) {
            return (
                <div className="section" key={this.props.listItem.id} ref={this.editListRef}>
                    <h4>Edit Movie</h4>
                    <ListItemForm
                        onFormSubmit={this._onFormSubmit}
                        listItem={this.props.listItem}
                        onCheckboxChange={this._onCheckboxChange}
                        onCancelClick={this._onCancelClick}
                    />
                </div>
            );
        } else if (this.state.isItemDetailsCardRendered) {
            return (
                <ListItemDetails
                    listItem={this.props.listItem}
                    onItemDetailsDisplay={this._onItemDetailsDisplay}
                    renderButtons={this._renderButtons}
                />
            );
        }
        return (
            <li className="row" key={this.props.listItem.id}>
                <span className="ListItem-itemInListHeading col s3 m4" onClick={this._onItemDetailsDisplay}>
                    {this.props.listItem.title}
                </span>
                <span className="col s1 m1">{this._makeDashInsteadOfZero(this.props.listItem.year)}</span>
                <span className="col s2 m2 isDesktop">{this._makeDashInsteadOfZero(this.props.listItem.genre)}</span>
                <span className="col s2 m1">{this._makeDashInsteadOfZero(this.props.listItem.rating)}</span>
                <span className="col s2 m1">
                    <label>
                        <input
                            type="checkbox"
                            checked={this.props.listItem.seen ? 'checked' : ''}
                            onChange={this._onCheckboxChange}
                        />
                        <span></span>
                    </label>
                </span>
                {this._renderButtons()}
            </li>
        );
    }

    render() {
        return <div className="ListItem-container">{this.renderListItem()}</div>;
    }

    _onFormSubmit = event => {
        event.preventDefault();
        const listItem = {
            id: this.props.listItem.id,
            title: event.target.elements.title.value,
            year: event.target.elements.year.value,
            genre: event.target.elements.genre.value,
            image: event.target.elements.image.value,
            rating: event.target.elements.rating.value,
            actors: event.target.elements.actors.value,
            director: event.target.elements.director.value,
            plot: event.target.elements.plot.value,
            seen: this.state.listItem.seen
        };
        this.props.onSaveListItem(listItem.id, listItem);
        this.setState({
            listItem,
            isEditing: false
        });
    };

    _onCancelClick = () => {
        this.setState({
            isEditing: false
        });
    };

    _onCheckboxChange = () => {
        this.props.onCheckboxChange(this.props.listItem.id);
        this.setState(state => ({
            listItem: {
                ...state.listItem,
                seen: !state.listItem.seen
            }
        }));
    };

    _onEditButtonClick = () => {
        this.setState({
            listRef: this._setListRef,
            isEditing: true
        });
    };

    _onDeleteButtonClick = () => {
        this.props.onRemoveListItem(this.props.listItem.id);
    };

    _onItemDetailsDisplay = () => {
        this.setState({
            isItemDetailsCardRendered: !this.state.isItemDetailsCardRendered
        });
    };

    _onEscapeClick = event => {
        if (event.keyCode === 27) {
            this.setState({
                isItemDetailsCardRendered: false
            });
        }
    };

    _renderButtons = () => {
        return (
            <span className="ListItem-buttonsBlock">
                <button
                    className="waves-effect waves-light btn-small light-blue lighten-2 isDesktop"
                    onClick={this._onEditButtonClick}
                >
                    Edit
                </button>
                <button
                    className="waves-effect waves-light btn-small light-blue lighten-2 isMobile ListItem-button"
                    onClick={this._onEditButtonClick}
                >
                    <i className="material-icons">edit</i>
                </button>
                <button
                    className="waves-effect waves-light btn-small red lighten-3 isDesktop"
                    onClick={this._onDeleteButtonClick}
                >
                    Delete
                </button>
                <button
                    className="waves-effect waves-light btn-small red lighten-3 isMobile ListItem-button"
                    onClick={this._onDeleteButtonClick}
                >
                    <i className="material-icons">delete</i>
                </button>
            </span>
        );
    };

    _makeDashInsteadOfZero(value) {
        if (value === 0 || value === '0' || value === 'N/A') {
            return '-';
        }
        return value;
    }
}
