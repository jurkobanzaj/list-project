import React from 'react';
import './ListItemDetails.scss';

const ListItemDetails = ({ listItem, onItemDetailsDisplay, renderButtons }) => (
    <div className="ListItemDetails-itemDetailsContainer">
        <div className="row">
            <span className="ListItemDetails-hideDetails col s1 offset-s11" onClick={onItemDetailsDisplay}>
                <i className="material-icons">close</i>
            </span>
            {isValueExists(listItem.image) && (
                <div className="col s3 m3">
                    <img className="ListItemDetails-poster" src={listItem.image} alt={`${listItem.title} poster`} />
                </div>
            )}
            <div className="col s9 m9">
                <div className="col s12 m12">
                    <h4 className="ListItemDetails-itemHeading">{listItem.title}</h4>
                </div>
                {isValueExists(listItem.year) && (
                    <span className="ListItemDetails-itemDetailsText col s12 m12">{listItem.year}</span>
                )}
                {isValueExists(listItem.genre) && (
                    <span className="ListItemDetails-itemDetailsText col s12 m12">{listItem.genre}</span>
                )}
                {isValueExists(listItem.rating) && (
                    <span className="ListItemDetails-itemDetailsText col s12 m12">
                        <span className="ListItemDetails-itemDetailsTextLegend">IMDB Rating:</span> {listItem.rating}
                    </span>
                )}
                {isValueExists(listItem.actors) && (
                    <span className="ListItemDetails-itemDetailsText col s12 m12">
                        <span className="ListItemDetails-itemDetailsTextLegend">Actors:</span> {listItem.actors}
                    </span>
                )}
                {isValueExists(listItem.director) && (
                    <span className="ListItemDetails-itemDetailsText col s12 m12">
                        <span className="ListItemDetails-itemDetailsTextLegend">Director:</span> {listItem.director}
                    </span>
                )}
                {isValueExists(listItem.plot) && (
                    <span className="ListItemDetails-itemDetailsText col s12 m12">
                        <span className="ListItemDetails-itemDetailsTextLegend">Plot:</span> {listItem.plot}
                    </span>
                )}
                {renderButtons()}
            </div>
        </div>
    </div>
);

function isValueExists(value) {
    return Boolean(value && value !== 0 && value !== '0' && value !== 'N/A');
}

export default ListItemDetails;
