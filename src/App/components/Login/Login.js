import React from 'react';
import { connect } from 'react-redux';
import { startLogin } from '../../actions/auth';
import './Login.scss';

const LoginPage = ({ startLogin }) => (
    <div className="row">
        <div className="LoginPage-container">
            <h3>Please login to start using this application</h3>
            <button onClick={startLogin} className="waves-effect waves-light btn">
                Login
            </button>
        </div>
    </div>
);

const mapDispatchToProps = dispatch => ({
    startLogin: () => dispatch(startLogin())
});

export default connect(
    undefined,
    mapDispatchToProps
)(LoginPage);
