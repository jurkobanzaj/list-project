import React from 'react';

const Footer = () => (
    <div className="Footer-container row">
        <p>© Yuriy Piddubnyak, 2018-2020</p>
    </div>
);

export default Footer;
