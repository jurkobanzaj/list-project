import React from 'react';
import { Link } from 'react-router-dom';

const NotFoundPage = () => (
    <div className="NotFound-container container">
        <div className="row">
            404! - <Link to="/home">Go home!</Link>
        </div>
    </div>
);

export default NotFoundPage;
