export const API_SEARCH_URL = 'https://www.omdbapi.com/?s=';
export const API_GET_MOVIE_DATA_BY_IMDB_ID_URL = 'https://www.omdbapi.com/?i=';

export const API_KEY = '&apikey=thewdb';
