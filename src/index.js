import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import AppRouter, { history } from './App/routers/AppRouter';
import Spinner from './App/components/Spinner/spinner';
import configureStore from './App/store/configureStore';
import { startSetList } from './App/actions/listActions';
import { startSetService } from './App/actions/serviceActions';
import { login, logout } from './App/actions/auth';
import { firebase } from './App/firebase/firebase.js';

const store = configureStore();
var hasRendered = false;
const renderApp = () => {
	if (!hasRendered) {
		render(<App />, document.getElementById('root'));
		hasRendered = true;
	}
};

class App extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<AppRouter />
			</Provider>
		)
	}
};

render(<Spinner />, document.getElementById('root'));

firebase.auth().onAuthStateChanged((user) => {
	if(user) {
		store.dispatch(login(user.displayName, user.uid));
		store.dispatch(startSetService());
		store.dispatch(startSetList()).then(() => {
			renderApp();
			if (history.location.pathname === '/') {
				history.push('/home')
			}
		})
	} else {
		store.dispatch(logout());
		renderApp();
		history.push('/');
	}
});
